/*Galen Leake
  Fall 2016
  CS 46 */

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>

int main(int argc, char **argv)
{
    struct sockaddr_in sa;
    int sockfd;
    
    printf("Starting\n");
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s host path\n", argv[0] );
        fprintf(stderr, "Example: www.csus.edu /\n", argv[0] ); //use "./weather api.wunderground.com/api/ca51a08ea2deeeca/conditions/forecast10day/q/95677.json /"
        exit(1);
    }
    
    printf("Looking up IP address\n");
    struct hostent *he = gethostbyname(argv[1] );
    char *ip = he->h_addr_list[0];
    printf("IP address is %s\n", ip);
    
    printf("%d.%d.%d.%d\n", (unsigned char)ip[0], (unsigned char)ip[1], (unsigned char)ip[2], (unsigned char)ip[3]);
    
    printf("Creating structure\n");
    sa.sin_family = AF_INET;
    sa.sin_port = htons(80);
    sa.sin_addr = *((struct in_addr *)ip);
    
    printf("Creating socket");
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
        fprintf(stderr, "Can't create socket\n");
        exit(3);
    }
    printf("Connecting\n");
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
        exit(2);
    }
    
    //original
    /*printf("Sending GET\n");
    char s[100];
    sprintf(s, "GET %s HTTP/1.0\n", argv[2]);
    send(sockfd, s, strlen(s), 0);
    sprintf(s, "Host: %s\n", argv[1]);
    send(sockfd, s, strlen(s), 0);
    sprintf(s, "\n");
    send(sockfd, s, strlen(s), 0);
    printf("Receiving response\n");
    char buf[1000];
    size_t size;*/
    
    //web2.c
    FILE *s = fdopen(sockfd, "r+");
    printf("Sending GET %d\n", argv[2]);
    fprintf(s, "GET %s HTTP/1.0\n", argv[2]);
    fprintf(s, "HOST: %s\n", argv[1]);
    fprintf(s, "\n");
    
    printf("Getting response\n");
    char line[1000];
    int count = 0;
    while (fgets(line, 1000, s) != NULL)
    {
        printf("%s", line);
        count++;
    }
    
    fclose(s);
    
    //Lecture notes replacing lines 58 to 64 in handout
    /*FILE *f = fopen(sockfd, "r+");
    while (fgets(buf, 1000, f) != NULL)
    {
        if (strstr(buf, "<img") != NULL)
        printf("%s", buf);
    }
    fclose(f);*/
    
    /*int count = 0;
    while ((size = recv(sockfd, buf, 1000, 0)) > 0)
    {
        printf("Recieving chunk %d of size %d\n", count, size);
        count++;
        fwrite(buf, size, 1, stdout);
    }*/
    close(sockfd);
}


/*
struct hostent {
    char    *h_name;        //official name of host
    char    **h_aliases;    // alias list
    int     h_addrtype;     //host address type
    int     h_length;       //length of address
    char    **h_addr_list;  //list of addresses
};

struct sockaddr_in {
    short       sin_family;     //e.g. AF_INET
    unsigned    sin_port;       //e.g. htons(3490)
    struct      in_addr;        //see struct in_addr, below
    char        sin_zero[8];    //zero this if you want to
};

struct in_addr {
    unsigned long s_addr;   //load with inet_aton()
};
*/